Get system information
======================

.. http:get:: /<api_url_prefix>/<api_version>/sys/info/

    Get system information

    **Example request**:

    .. sourcecode:: http

        GET /<api_url_prefix>/<api_version>/sys/info/ HTTP/1.1
        Host: example.com
        Accept: application/json, text/javascript

    **Example response**:

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Vary: Accept
        Content-Type: text/javascript

        {
            "manufacturer": "Legrand",
            "product": "Cam model x",
            "sensor": "Sensor ref-7852"
            "firmware": "1.2.5-rc5",
            "platform": "iMX6-leg-motherboard ref: 865245"
        }

    :reqheader Accept: the response content type depends on
                       :mailheader:`Accept` header
    :reqheader Authorization: optional OAuth token to authenticate
    :resheader Content-Type: this depends on :mailheader:`Accept`
                            header of request
    :statuscode 200: no error
    :statuscode 404: information not available
