Get system health
=================

.. http:get:: /<api_url_prefix>/<api_version>/sys/health/

    Get system health

    **Example request**:

    .. sourcecode:: http

        GET /<api_url_prefix>/<api_version>/sys/health/ HTTP/1.1
        Host: example.com
        Accept: application/json, text/javascript

    **Example response**:

    .. sourcecode:: http

        HTTP/1.1 200 OK
        Vary: Accept
        Content-Type: text/javascript

        {
            "cpu_load": "0.89",
            "disk_total": "256 MB"
            "disk_free": "100 MB",
        }

    :reqheader Accept: the response content type depends on
                       :mailheader:`Accept` header
    :reqheader Authorization: optional OAuth token to authenticate
    :resheader Content-Type: this depends on :mailheader:`Accept`
                            header of request
    :statuscode 200: no error
    :statuscode 404: information not available
